

{

    init: function (elevators, floors) {

        var floorButtons = [];
        var stoppedForPassenger = 0;

        for (let floor of floors) {

            floorButtons.push({
                floorNo: floor.floorNum()
            });

            floor.on("up_button_pressed", function () {

                let index = floorButtons.findIndex((floorButton) => floorButton.floorNo == floor.floorNum());
                floorButtons[index] = {
                    floorNo: floor.floorNum(),
                    pressed: true
                };
                let mostEmptyElevator = getMostEmptyElevator();
                elevators[mostEmptyElevator].goToFloor(floor.floorNum(), true);
                if (mostEmptyElevator == 1){
                    console.info("Adding destination:" + floor.floorNum() + ", to elevator"+ mostEmptyElevator +" because of up press down on floor");
                    console.info("Destination now:" + elevators[mostEmptyElevator].destinationQueue);
                }

            });

            floor.on("down_button_pressed", function () {

                let index = floorButtons.findIndex((floorButton) => floorButton.floorNo == floor.floorNum());
                floorButtons[index] = {
                    floorNo: floor.floorNum(),
                    pressed: true
                };
                let mostEmptyElevator = getMostEmptyElevator();
                elevators[mostEmptyElevator].goToFloor(floor.floorNum(), true);
                if (mostEmptyElevator == 1){
                    console.info("Adding destination:" + floor.floorNum() + ", to elevator"+ mostEmptyElevator +" because of up press down on floor");
                    console.info("Destination now:" + elevators[mostEmptyElevator].destinationQueue);
                }

            })
        }
       
       // elevators[2].goToFloor(4);
       // elevators[3].goToFloor(5);
        let elIndex = -1;
        for (let elevator of elevators) {
            elIndex += 1;
            // initial spread elevators


            elevator.on("stopped_at_floor", function (floorNum) {
                //console.info("Clearing button state for:" + floorNum + ", because of elevator stopped there");
                let index = floorButtons.findIndex((floorButton) => floorButton.floorNo == floorNum);
                floorButtons[index] = {
                    floorNo: floorNum,
                    pressed: false
                };
            });

            // go to floor when button pressed in elevator
            elevator.on("floor_button_pressed", function (floorNum) {
                
                elevator.goToFloor(floorNum);
                if (elIndex == 1){
                    console.info("Adding destination:" + floorNum + ", to elevator"+ elIndex +" because of button in elevator");
                    console.info("Destination now:" + elevator.destinationQueue);
                }
            });

            // passing floor behaviour optimise
            elevator.on("passing_floor", function (floorNum, direction) {

                /* let nextDestination = elevator.destinationQueue[0];
                if (nextDestination > floorNum){
                    elevator.goingUpIndicator(true);
                    elevator.goingDownIndicator(false);
                }
                else if (nextDestination < floorNum) {
                    elevator.goingUpIndicator(false);
                    elevator.goingDownIndicator(true);
                }
                else {
                     elevator.goingUpIndicator(true);
                     elevator.goingDownIndicator(true);
                }*/

                let floorButton = floorButtons.find((floorButton) => floorButton.floorNo == floorNum);

                if (floorButton.pressed) {
                    // if ((direction == 'up' && elevator.destinationQueue[0] > floorNum) ||
                    //    (direction == 'down' && elevator.destinationQueue[0] < floorNum)){
                    if(elevator.loadFactor() < 0.6) {
                        stoppedForPassenger += 1;
                      
                        clearDestination(floorNum);
                        elevator.goToFloor(floorNum, true);
                         if (elIndex == 1){
                            console.info("Immidiatly stooping at :" + floorNum + ", for elevator"+ elIndex +" because of passing floor with pressed button");
                            console.info("Destination now:" + elevator.destinationQueue);
                        }
                    }
                    //}
                } 
                if (elevator.getPressedFloors().includes(floorNum)) {
                  
                    clearDestination(floorNum);
                    elevator.goToFloor(floorNum, true);
                     if (elIndex == 1){
                        console.info("Immidiatly stooping at :" + floorNum + ", for elevator"+ elIndex +" because of passing floor which is pressed in elevator");
                        console.info("Destination now:" + elevator.destinationQueue);
                    }
                }


            });

        }


        // clear destination of every elevator
        clearDestination = (floorToClear) => {
            let elIndex = -1;
            for (let elevator of elevators) {
                elIndex += 1;
                if (elevator.getPressedFloors().includes(floorToClear)){
                    if (elIndex == 1){
                        console.info("called to clear:"+ floorToClear+ " but button pressed in elevator... not clearing");
                    }
                }
                else {
                    if (elIndex == 1 && elevator.destinationQueue.includes(floorToClear)){
                        console.info("Clearing destination queue for floor:"+ floorToClear+ " because other elevator stopped there.");
                        console.info("Destination now:" + elevator.destinationQueue);   
                    }
                    elevator.destinationQueue = elevator.destinationQueue.filter(floorDest => floorDest !== floorToClear);
                    elevator.checkDestinationQueue();
                }
            }
        }

        getMostEmptyElevator = () => {
        
            let minLoadFactor = 20;
            let index = 0;
            for (let elevator of elevators) {
                if (elevator.loadFactor() < minLoadFactor){
                    minLoadFactor = elevator.loadFactor();
                    mostEmptyElevator = index;
                };
                index =+1;
            }
            return index;
        }



    },
        update: function (dt, elevators, floors) {
            // We normally don't need to do anything here
        },

}